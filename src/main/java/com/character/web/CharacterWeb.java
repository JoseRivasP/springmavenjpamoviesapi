/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.character.web;

import com.character.dao.CharacterDao;
import com.character.domain.CharacterDomain;
import java.util.*;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DellUser
 */
@RestController
public class CharacterWeb {
    @Autowired
    private CharacterDao characterDao;
    
    @GetMapping("/characters")
    public List<CharacterDomain> getCharacters(){
        List<CharacterDomain> data = characterDao.findAll();
        return data;
    }
    
    @PostMapping("/characters")
    public String saveCharacter(@Valid CharacterDomain character, Errors errores){
        boolean id = character.getId() != null;
        boolean imagen = character.getImagen() != null;
        boolean nombre = character.getNombre() != null;
        boolean edad = character.getEdad() != null;
        boolean peso = character.getPeso() != null;
        boolean historia = character.getHistoria() != null;
        boolean peliculasseries = character.getPeliculasseries() != null;
               
        if(character.getPeliculasseries().equals("") || character.getPeliculasseries().equals("delete")){
            character.setPeliculasseries(null);
        }
                
        if(errores.hasErrors() || id || !imagen || !nombre || !edad || !peso || !historia || !peliculasseries){
            return "No saved";
        }
        
        characterDao.saveAndFlush(character);
        return "saved";
    }
    
    @PutMapping("/characters/{id}")
    public String updateCharacter(@Valid CharacterDomain characterToUpdate, Errors errores){
        if(errores.hasErrors()){
            return "We couldn't update the information";
        }
        
        CharacterDomain characterToBeUpdated = characterDao.findById(characterToUpdate.getId()).get();
        
        
        String imagen = characterToUpdate.getImagen() != null ? characterToUpdate.getImagen() : characterToBeUpdated.getImagen();
        String nombre = characterToUpdate.getNombre() != null ? characterToUpdate.getNombre() : characterToBeUpdated.getNombre();
        String edad = characterToUpdate.getEdad() != null ? characterToUpdate.getEdad() : characterToBeUpdated.getEdad();
        String peso = characterToUpdate.getPeso() != null ? characterToUpdate.getPeso() : characterToBeUpdated.getPeso();
        String historia = characterToUpdate.getHistoria() != null ? characterToUpdate.getHistoria() : characterToBeUpdated.getHistoria();
        String peliculasSeries = characterToUpdate.getPeliculasseries() != null && !characterToUpdate.getPeliculasseries().equals(characterToBeUpdated.getPeliculasseries()) ? getPeliculasSeriesHelper(characterToBeUpdated.getPeliculasseries(),characterToUpdate.getPeliculasseries()) : characterToBeUpdated.getPeliculasseries();
        
        characterToBeUpdated.setImagen(imagen);
        characterToBeUpdated.setNombre(nombre);
        characterToBeUpdated.setEdad(edad);
        characterToBeUpdated.setPeso(peso);
        characterToBeUpdated.setHistoria(historia);
        characterToBeUpdated.setPeliculasseries(peliculasSeries);
        
        characterDao.saveAndFlush(characterToBeUpdated);
        
        return "updated";
    }
    
    @DeleteMapping({"/characters","/characters/{id}"})
    public String DeleteCharacters(@Valid CharacterDomain character, Errors errores){
        if(errores.hasErrors() || character.getId() == null || !characterDao.existsById(character.getId())){
            return "We couldn't deleted the character";
        }
        
        System.out.println(character.getId());
        characterDao.deleteById(character.getId());
        return "Deleted";
    }
    
    public String getPeliculasSeriesHelper(String currentString, String newString){
        if(currentString == null) {
            currentString = newString;
        }
        
        List<String> currentString2 = new ArrayList<>(Arrays.asList(currentString.split(","))); 
        
        for(int i = 0; i < newString.length(); i++){
            String lyric = String.valueOf(newString.charAt(i));         
            if(currentString2.contains(lyric)){
                continue;
            }
            
            if(newString.equals("delete")){
                currentString2.removeAll(currentString2);
            }
            
            currentString2.add(lyric);
        }
        currentString2.remove("e");
        currentString2.remove(",");
        Collections.sort(currentString2);
        return currentString2.size() >= 1 ? String.join(",", currentString2) : null;
    }
    
}
