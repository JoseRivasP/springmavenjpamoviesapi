
package com.character.web;

import com.character.dao.CharacterDao;
import com.character.dao.GenresDao;
import com.character.domain.CharacterDomain;
import com.character.domain.GenresDomain;
import com.character.domain.MoviesDomain;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GenresWeb {
        @Autowired
    private GenresDao genresDao;
    
    @GetMapping("/genres")
    public List<GenresDomain> getCharacters(){
        List<GenresDomain> data = genresDao.findAll();
        return data;
    }
    
    @PostMapping("/genres")
    public String saveCharacter(@Valid GenresDomain genres, Errors errores){
        boolean id = genres.getId() != null;
        boolean imagen = genres.getImagen() != null;
        boolean nombre = genres.getNombre() != null;
        boolean moviesAsociadas = genres.getMoviesAsociadas() != null;
               
        if(genres.getMoviesAsociadas().equals("") || genres.getMoviesAsociadas().equals("delete")){
            genres.setMoviesAsociadas(null);
        }
                
        if(errores.hasErrors() || id || !imagen || !nombre || !moviesAsociadas){
            return "No saved";
        }
        
        genresDao.saveAndFlush(genres);
        return "saved";
    }
    
    @PutMapping("/genres/{id}")
    public String updateCharacter(@Valid GenresDomain genresToUpdate, Errors errores){
        if(errores.hasErrors()){
            return "We couldn't update the information";
        }
        
        GenresDomain genresToBeUpdated = genresDao.findById(genresToUpdate.getId()).get();
        
        
        String imagen = genresToUpdate.getImagen() != null ? genresToUpdate.getImagen() : genresToBeUpdated.getImagen();
        String nombre = genresToUpdate.getNombre() != null ? genresToUpdate.getNombre() : genresToBeUpdated.getNombre();
        String moviesAsociadas = genresToUpdate.getMoviesAsociadas()!= null && !genresToUpdate.getMoviesAsociadas().equals(genresToBeUpdated.getMoviesAsociadas()) ? getPeliculasSeriesHelper(genresToBeUpdated.getMoviesAsociadas(),genresToUpdate.getMoviesAsociadas()) : genresToBeUpdated.getMoviesAsociadas();
        
        genresToBeUpdated.setImagen(imagen);
        genresToBeUpdated.setNombre(nombre);
        genresToBeUpdated.setMoviesAsociadas(moviesAsociadas);
        
        genresDao.saveAndFlush(genresToBeUpdated);
        
        return "updated";
    }
    
    @DeleteMapping({"/genres","/genres/{id}"})
    public String DeleteCharacters(@Valid GenresDomain genres, Errors errores){
        if(errores.hasErrors() || genres.getId() == null || !genresDao.existsById(genres.getId())){
            return "We couldn't deleted the character";
        }
        
        genresDao.deleteById(genres.getId());
        return "Deleted";
    }
    
    public String getPeliculasSeriesHelper(String currentString, String newString){
        if(currentString == null) {
            currentString = newString;
        }
        
        List<String> currentString2 = new ArrayList<>(Arrays.asList(currentString.split(","))); 
        
        for(int i = 0; i < newString.length(); i++){
            String lyric = String.valueOf(newString.charAt(i));         
            if(currentString2.contains(lyric)){
                continue;
            }
            
            if(newString.equals("delete")){
                currentString2.removeAll(currentString2);
            }
            
            currentString2.add(lyric);
        }
        currentString2.remove("e");
        currentString2.remove(",");
        Collections.sort(currentString2);
        return currentString2.size() >= 1 ? String.join(",", currentString2) : null;
    }
}
