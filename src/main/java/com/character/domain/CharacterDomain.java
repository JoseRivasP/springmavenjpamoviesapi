package com.character.domain;

import java.io.Serializable;
import javax.persistence.*;



@Entity
@Table(name = "characters")
public class CharacterDomain implements Serializable{
        
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    private String imagen;

    private String nombre;

    private String edad;
    
    private String peso;

    private String historia;
    
    private String peliculasseries;

    public Integer getId() {
        return id;
    }

    public String getImagen() {
        return imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEdad() {
        return edad;
    }

    public String getPeso() {
        return peso;
    }

    public String getHistoria() {
        return historia;
    }

    public String getPeliculasseries() {
        return peliculasseries;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

    public void setPeliculasseries(String peliculasseries) {
        this.peliculasseries = peliculasseries;
    }
}
