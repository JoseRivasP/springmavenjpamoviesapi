/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.character.dao;

import com.character.domain.MoviesDomain;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author DellUser
 */
public interface MoviesDao extends JpaRepository<MoviesDomain, Integer>  {
    @Override
    public List<MoviesDomain> findAll();
}
