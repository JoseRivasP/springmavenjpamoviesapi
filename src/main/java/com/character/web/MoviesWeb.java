/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.character.web;

import com.character.dao.MoviesDao;
import com.character.domain.MoviesDomain;
import java.util.*;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DellUser
 */
@RestController
public class MoviesWeb {

    @Autowired
    private MoviesDao moviesDao;

    @GetMapping("/movies")
    public List<MoviesDomain> getCharacters() {
        List<MoviesDomain> movies = moviesDao.findAll();
        return movies;
    }

    @PostMapping("/movies")
    public String saveCharacter(@Valid MoviesDomain movies, Errors errores) {
        boolean id = movies.getId() != null;
        boolean imagen = movies.getImagen() != null;
        boolean titulo = movies.getTitulo() != null;
        boolean fechaDeCreacion = movies.getFechaDeCreacion() != null;
        boolean calificacion = movies.getCalificacion() != null;
        boolean personajesAsociados = movies.getPersonajesAsociados() != null;

        if (movies.getPersonajesAsociados().equals("") || movies.getPersonajesAsociados().equals("delete")) {
            movies.setPersonajesAsociados(null);
        }

        if (errores.hasErrors() || id || !imagen || !titulo || !fechaDeCreacion || !calificacion || !personajesAsociados) {
            return "No saved";
        }

        moviesDao.saveAndFlush(movies);
        return "saved";
    }

    @PutMapping("/movies/{id}")
    public String updateCharacter(@Valid MoviesDomain moviesToUpdate, Errors errores) {
        if (errores.hasErrors()) {
            return "We couldn't update the information";
        }

        MoviesDomain moviesToBeUpdated = moviesDao.findById(moviesToUpdate.getId()).get();

        String imagen = moviesToUpdate.getImagen() != null ? moviesToUpdate.getImagen() : moviesToBeUpdated.getImagen();
        String titulo = moviesToUpdate.getTitulo() != null ? moviesToUpdate.getTitulo() : moviesToBeUpdated.getTitulo();
        String fechaDeCreacion = moviesToUpdate.getFechaDeCreacion() != null ? moviesToUpdate.getFechaDeCreacion() : moviesToBeUpdated.getFechaDeCreacion();
        String calificacion = moviesToUpdate.getCalificacion() != null ? moviesToUpdate.getCalificacion() : moviesToBeUpdated.getCalificacion();
        String personajesAsociados = moviesToUpdate.getPersonajesAsociados() != null && !moviesToUpdate.getPersonajesAsociados().equals(moviesToBeUpdated.getPersonajesAsociados()) ? getPersonajesAsociados(moviesToBeUpdated.getPersonajesAsociados(), moviesToUpdate.getPersonajesAsociados()) : moviesToBeUpdated.getPersonajesAsociados();

        moviesToBeUpdated.setImagen(imagen);
        moviesToBeUpdated.setTitulo(titulo);
        moviesToBeUpdated.setFechaDeCreacion(fechaDeCreacion);
        moviesToBeUpdated.setCalificacion(calificacion);
        moviesToBeUpdated.setPersonajesAsociados(personajesAsociados);

        moviesDao.saveAndFlush(moviesToBeUpdated);

        return "updated";
    }

    @DeleteMapping({"/movies", "/movies/{id}"})
    public String DeleteMovies(@Valid MoviesDomain movies, Errors errores) {
        if (errores.hasErrors() || movies.getId() == null || !moviesDao.existsById(movies.getId())) {
            return "We couldn't deleted the character";
        }

        moviesDao.deleteById(movies.getId());
        return "Deleted";
    }

    public String getPersonajesAsociados(String currentString, String newString) {
        if (currentString == null) {
            currentString = newString;
        }

        List<String> currentString2 = new ArrayList<>(Arrays.asList(currentString.split(",")));

        for (int i = 0; i < newString.length(); i++) {
            String lyric = String.valueOf(newString.charAt(i));
            if (currentString2.contains(lyric)) {
                continue;
            }

            if (newString.equals("delete")) {
                currentString2.removeAll(currentString2);
            }

            currentString2.add(lyric);
        }
        currentString2.remove("e");
        currentString2.remove(",");
        Collections.sort(currentString2);
        return currentString2.size() >= 1 ? String.join(",", currentString2) : null;
    }
}
