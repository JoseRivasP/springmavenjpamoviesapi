/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.character.dao;

import com.character.domain.GenresDomain;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenresDao extends JpaRepository<GenresDomain, Integer> {

    @Override
    public List<GenresDomain> findAll();
}
