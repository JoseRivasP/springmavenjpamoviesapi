
package com.character.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "genres")
public class GenresDomain implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    private String imagen;

    private String nombre;
    
    private String moviesAsociadas;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setMoviesAsociadas(String moviesAsociadas) {
        this.moviesAsociadas = moviesAsociadas;
    }

    public Integer getId() {
        return id;
    }

    public String getImagen() {
        return imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public String getMoviesAsociadas() {
        return moviesAsociadas;
    }
}
