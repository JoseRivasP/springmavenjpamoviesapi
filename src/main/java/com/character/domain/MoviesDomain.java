package com.character.domain;

import java.io.Serializable;
import javax.persistence.*;



@Entity
@Table(name = "movies")
public class MoviesDomain implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    private String imagen;

    private String titulo;

    private String fechaDeCreacion;
    
    private String calificacion;

    private String personajesAsociados;

    public Integer getId() {
        return id;
    }

    public String getImagen() {
        return imagen;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getFechaDeCreacion() {
        return fechaDeCreacion;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public String getPersonajesAsociados() {
        return personajesAsociados;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setFechaDeCreacion(String fechaDeCreacion) {
        this.fechaDeCreacion = fechaDeCreacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public void setPersonajesAsociados(String personajesAsociados) {
        this.personajesAsociados = personajesAsociados;
    }
    
    
}
